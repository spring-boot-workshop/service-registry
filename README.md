# 07 Service Registry

Deine Aufgabe ist es, eine Eureka Service Registry App zu erstellen. 
In dieser Service Registry werden sich dann unser Product-Service, sowie das API-Gateway registrieren.

## Aufgabe 1
Los geht es auf dem Master Branch. Deine Aufgabe ist es die bestehende Klasse ServiceDiscoveryApplication zu erweitern,
sodass die App eine Eureka Service Registry startet. 

Die notwendigen Schritte findest du in der [Dokumentation](https://cloud.spring.io/spring-cloud-netflix/reference/html/#spring-cloud-eureka-server).

Wenn du fertig bist, sollte deine Applikation unter folgender URL verfügbar sein:
    
    http://localhost:8084/
    
## Aufgabe 2
Das API-Gateway nutzt die hardcodierten URLs für den Product-Service und den Shopping-Cart-Service. 
Die URL für den Product-Service soll durch die Eureka Service Registry aufgelöst werden. 

### Aufgabe 2.1
Erweitere hierzu als erstes den Product-Service um einen Eureka-Client, 
mit dem dieser sich an der Ereka Service Registry registriert. 
Die notwendigen Schritte findest du in der [Dokumentation](https://cloud.spring.io/spring-cloud-netflix/reference/html/#registering-with-eureka).

Wenn du fertig bist solltest du den Product-Service als neue Applikation in der Eureka Service Registry sehen:

![eureka-registry](img/eureka-registry.png) 

## Aufgabe 2.2
Als nächstes gilt die URL des Product-Service im API-Gateway durch den Service-Name des Product-Service zu ersetzen.
Entferne dazu den URL-Parameter aus der `@FeignClient` Annotation. 

Die notwendigen Schritte findest du in der [Dokumentation](https://cloud.spring.io/spring-cloud-netflix/multi/multi_spring-cloud-feign.html).
